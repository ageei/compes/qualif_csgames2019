# Qualification de l'AGEEI pour les CS Games 2019 

Cette année encore, l'AGEEI participera aux [CS Games][1]; une
compétition informatique touchant à différents aspects du domaine.
Les membres de notre délégation seront sélectionnés parmis les
[candidatures][2] reçues et accompagnées de vos solutions pour les
épreuves qualificatives présentent dans ce dépôt.

Si une épreuve vous intéresse et qu'il n'y a pas d'épreuves
qualificatives pour celle-ci (ou que vous voulez nous en montrer plus!),
n'hésitez pas à nous partager quelque chose qui s'aligne avec
l'épreuve et qui saura vous démarquer. Aussi, toutes les épreuves
officielles n'ont pas encore été annoncées. On peut s'attendre à voir
annoncer des épreuves de développement d'application mobile, système
d'exploitation, sécurité informatique, par exemple. N'hésitez donc pas à
nous partager des projets que vous avez accomplis et dont vous êtes
fier!

Vos solutions devront être entreposées dans un dépôt **privé** sur
GitLab ou GitHub. Sur GitLab, veuillez ajouter @pgregoire et, sur
GitHub, antifob, comme collaborateur. Vous pouvez aussi envoyer un
zip de votre solution à l'adresse [competition@ageei.org][3].

On vous invite **fortement** à rejoindre le canal `#cs-games` sur Slack
pour vous tenir au courant des derniers développement.

N'hésitez pas à communiquer avec corinnep, fob ou mplessard sur Slack
si vous avez des questions ou commentaires.

[1]: http://2019.csgames.org/
[2]: https://goo.gl/forms/hllG1dG7RJrVW7i62
[3]: mailto:competition@ageei.org
