#include <stdio.h>
#include <string.h>

void rm_cr(char *pass_in, int cap_ln) {
	int pos = 0;
	while (pos < cap_ln) {
		char next = pass_in[pos];
		if (next == '\0')
			break;
		if (next == '\n') {
			pass_in[pos] = '\0';
			break;
		}
		pos++;
	}
	if (pos >= cap_ln) {
		pass_in[cap_ln - 1] = '\0';
	}
}


int main() {
	char expected[31] = {0xb9, 0xb3, 0xbe, 0xb8, 0x84, 0xc8, 0x97, 0xcb, 0xc8, 0xa0, 0x88, 0xcb, 0xca, 0xa0, 0x91, 0xcf, 0xc8, 0xa0, 0x97, 0xcb, 0x8d, 0x9b, 0xa0, 0xcb, 0xc8, 0xa0, 0xcb, 0xce, 0xce, 0xde, 0x82};
	char input[128];
	unsigned long curr;

	printf("Password?\n");
	fgets(input, 128, stdin);
	rm_cr(input, 128);

	//FLAG{7h47_w45_n07_h4rd_47_411!}
	if (strlen(input) != 31) {
		goto FAIL;
	}

	for (curr = 0; curr < 31; curr++) {
		if (expected[curr] != ~input[curr])
			goto FAIL;
	}

	printf("OK!\n");
	return 0;
FAIL:
	printf("WRONG.\n");
	return 1;
}
