# Hardware hacking

Pour la deuxième fois, les CS Games proposent une épreuve de Hardware Hacking.

Le temps et les informations nous manquant, il a été impossible de créer une 
épreuve de qualification faisant justice à la réelle compétition. 

Nous mettons donc à l'épreuve votre créativité: prouvez-nous que vous avez ce 
qu'il faut pour découvrir les secrets d'un tas de circuits. Montrez-nous les
ordinateurs que vous avez monté, démonté et remonté, les vitres de téléphone
changées, les petits projets Arduino fait durant le temps des fêtes ou même
votre installation maison de détecteur de plantes à arroser.

Sky's the limit but stay close to the metal!

