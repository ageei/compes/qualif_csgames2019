# Sécurité - Pwnpal

Pwnpal... Pwnpal... est-ce que c'est comme Paypal? Est-ce qu'on peut
payer avec des _flags_? Est-ce qu'on peut vendre des _flags_? Si tous
le monde mets ses flags sur Pwnpal, peut-être qu'il y a moyen de
récupérer un _flag_.

[PwnPal][1], envoyer des _flags_, payer en _flags_, vendez vos _flags_.


[1]: https://salty-caverns-69562.herokuapp.com/
