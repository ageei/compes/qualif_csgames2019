#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random


CMDSFMT = {
    '0': 0,
    'v': 99,
    '^': 1,
    '.': 0,
    'r': 1,
    '#': 0
}
CMDS  = list(CMDSFMT.keys())
NCMDS = len(CMDS)


r = random.Random()


def i():
    return '{}'.format(r.randint(0, 9999))


def g():
    c = CMDS[r.randint(0, NCMDS-1)]
    f = CMDSFMT[c]

    if 1 == f:
        c += ' {}'.format(i())
    elif 99 == f:
        c += ' {}'.format(' '.join([i() for _ in range(r.randint(0, 5))]))

    return c


def n(v):
    for _ in range(v):
        yield g()


if '__main__' == __name__:
    import sys
    print('\n'.join(n(100 if 1 == len(sys.argv) else int(sys.argv[1]))))
