# Sneaky snake

## Web

L'objectif du défi est de coder un jeu snake avec l'aide des canvas HTML5. 

On commence souvent à programmer dans un language en programmant un jeu snake
pour apprendre les bases du language. Donc, si vous êtes débutant et que vous
n'avez jamais créé de jeu snake, ne vous inquiétez pas, il existe une panoplie
de tutoriaux en ligne pour vous aider. L'objectif du défi est de vous permettre
d'apprendre les bases de la programmation web, mais si vous êtes déjà
expérimenté, rassurez-vous! Le défi est décomposé en plusieurs objectifs;
certains faciles, certains très difficiles, choisissez ceux que vous préférez! 

*Les objectifs et bonus ne sont pas tous précis, soyez créatif!*

## Objectif

Faire un jeu snake (vous avez carte blanche pour le style, l'implémentation
et tout le reste. On veut seulement un jeu snake fonctionnel!)
   
*N'hésistez pas à vous **inspirer** de 
[tutoriel](http://www.jdstraughan.com/2013/03/05/html5-snake-with-source-code-walkthrough/)*

## Bonus

1. Ajouter différentes sortes de bonbons (ce que le serpent mange pour grandir).
   Soyez original! ex: Vaut plus de point, rend invincible, permet d'aller plus
   vite...
2. Ajouter une balle que le serpent peut bouger avec son corps.
3. Ajouter un méchant démon qui bouge aléatoirement et qui tue le serpent
   s'ils sont superposés sur une case.

## Avis aux experts

Les défis précédents sont trop facile ? Ça ne vous a pris que 20 min ? Essayez
ceux-là!

1. Créer un AI pour que le démon poursuive le serpent. (Peut-être prévoir où
   le joueur va aller en fonction des bonbons sur la carte.)
2. Multijoueur. Jouez à deux sur un ordi, c'est faisable, mais pas pratique...
   Essayez par les internets! ;)

---

Si vous voulez rajoutez autre chose au jeu, allez y et listez le dans votre
README.md.

## Informations supplémentaires

- Il est attendu que les débutants s'inspirent grandement de tutoriel, mais
  évitez quand même le plagiat.
- Si vous souhaitez vous concentrer sur les défis d'experts, vous pouvez
  prendre le projet de quelqu'un sur git et citez **les sources**. (e.g. si vous
  êtes uniquement motivé par l'AI)
- Un débutant ayant des difficultés avec les tutoriels peut aussi utiliser le
  projet de quelqu'un d'autre et faire les bonus. (non-recommandé)
- Utiliser du code externe et "oublier" les sources pourrait provoquer la
  disqualification du candidat.
- Utiliser le README pour nous dire ce que vous avez implémenté et les
  instructions pour tester votre application facilement.

*Si vous n'êtes vraiment pas confortable avec la programmation web vous pouvez
utiliser le language et la plateforme de votre choix, mais ce n'est pas
l'objectif de cette épreuve.*
