# -*- coding: utf-8 -*-

from __future__ import print_function
from functools import reduce
import random
import sys


r = random.Random()
q = int(sys.argv[1]) if 1 != len(sys.argv) else 10000


ns = []
for i in range(q):
	ns.append(r.randint(-100000,100000))

print(', '.join(['{}'.format(n) for n in ns]))


def diff(l):
    return reduce(lambda a,b: a-b, l)

def produit(l):
    return reduce(lambda a,b: a*b, l)

def eprint(s):
    print(s, file=sys.stderr)

eprint('[I] somme: {}'.format(sum(ns)))
eprint('[I] différence: {}'.format(diff(ns)))
eprint('[I] produit: {}'.format(produit(ns)))
