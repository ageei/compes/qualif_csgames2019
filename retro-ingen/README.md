# Rétro-ingénierie

Dites-nous comment chacun des programmes à étudier fait, le processus
que vous avez suivi et, surtout, le _flag_ qu'il cache.
