# Programmation fonctionnelle

L'épreuve officielle exige la programmation en Haskell. Les exercices
suivants doivent être programmés dans ce langage. Veuillez numéroter
vos solutions.

1. Écrire un programme qui détermine si la chaîne de caractères passée
   en argument est un palindrôme.
2. Écrire un programme qui affiche le `n`-ième nombre de la suite de
   Fibonnaci; où `n` est un nombre passé en argument.
3. Écrire un programme qui trouve le 1er nombre de la suite de Fibonnaci
   qui contient `n` chiffres; où `n` étant un nombre passé en argument.
   On veut connaître le nombre de la suite, pas sa position.
4. Écrire un programme qui récupère et calcule la somme, la différence
   et le produit des nombres contenus dans le fichier passé en argument
   ou l'entrée standard si aucun fichier n'est spécifié. Le script
   `sdp.py` pourrait vous être utile.
5. Écrire un serveur `echod` et un client `echo` qui communique
   ensemble par connexion TCP. Le client doit récupérer chaque ligne
   écrite en entrée standard et les envoyer au serveur. Le client doit
   aussi afficher chaque ligne reçue par le serveur. Le serveur doit
   récupérer les messages envoyés par les clients et leurs renvoyés.
   Le serveur doit supporter plusieurs clients.
6. Écrire un programme qui manipule une pile en fonction des commandes
   passées sur l'entrée standard. Chaque commande est séparé par un
   caractère de nouvelle ligne (`\n`) et peut avoir des arguments,
   séparés par des espaces (` `). Les commandes sont `0` (vider la pile),
   `v` (mettre les arguments, en ordre, sur la pile), `^` (enlever `n`
   élément de la pile; où `n` est l'entier passé en argument à la
   commande), `.` (afficher la valeur au haut de la pile), `r`
   (remplacer la valeur au haut de la pile par la valeur passé en
   argument) et `#` (afficher toute la pile, avec chaque élément séparé
   par une nouvelle ligne). Le fichier `stk.py` peut être utilisé pour
   générer des séries de commandes. **La pile est toujours vide au
   début de l'exécution**.

Ne vous gênez pas pour ajouter des fonctionnalités **supplémentaires**.
