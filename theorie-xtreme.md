# Théorie Xtrême

On vous demande simplement de nous fournir l'implémentation du plus
d'éléments dans la liste suivante. Vous pouvez utiliser le langage
que vous désirez, mais ce doit être *votre* implémentation (pas
celle d'une librairie, d'une module ou de quelqu'un d'autre).

**Nous vérifierons chaque solution pour le plagiat. Tout plagiat verra
le candidat disqualifié. On peut vous demander de nous expliquer et
refaire votre code à des fins de vérification.**

## Arbres binaires

L'implémentation doit permettre l'insertion, la suppression et la
recherche dans l'arbre. Fournissez nous un programme qui utilise cet
implémentation.

- Arbre AVL
- Arbre B
- Arbre rouge-noir
- Arbre splay

## Listes

- Liste simplement chaînée
- Liste doublement chaînée

## Autres structures de données

- File d'attente max.
- File d'attente min.
- Pile

## Algorithmes

- Calcul de la distance Hamming d'une chaîne d'octets
- _quicksort_
- _bubblesort_
- _radix sort_
- Calcul le nombre de bits nul (0) dans une chaîne d'octets.
- Calcul le nombre de bits mis (1) dans une chaîne d'octets.

## Cryptographie et encodage

- Rotation ASCII alphabétique (`[a-zA-Z]`) de *n* caractères (0 <= *n* <= 26) (e.g. rot "1aA" 13 => "1nN")
- Rotation binaire de *n* valeurs (e.g. `brot [255,0] par 1 = [0,1]`)
- Rotation d'une chaîne de caractères de *n* positions (e.g. `srot "abc" par 1 = "bca"`)
- Encodeur/décodeur base32
- Encodeur/décodeur base64
- Encodeur/décodeur base85
- Chiffrement XOR d'une chaîne d'octets avec un octet comme clé
- Chiffrement XOR d'une chaîne d'octets avec une chaîne d'octets comme clé
- Fonction de _padding_ PKCS #7
- Compression/décompression _run-length encoding_ (https://en.wikipedia.org/wiki/Run-length_encoding)
- Compression/décompression LZ77.

*Les rotations peuvent être positives ou négatives:* `srot (srot "abc" 1) -1 = "abc"`
