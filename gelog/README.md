# Ingénierie logicielle

Votre tâche est de développer une solution au problème suivant. La
solution devra être développer selon les règles de l'art et être
composé de documents de conception, code et tests. Vous serez
également chargé de la maintenance du logiciel tout au long de sa
durée de vie. Vous devrez remettre le projet terminé avant le 23
janvier 23:59 afin de mettre le logiciel en production le lendemain.
Nous prévoyons une fin de vie pour le 28 janvier 12:00. D'ici là,
nous vous contacterons s'il y a des modifications à effectuer.

## Description du problème

Les frères et soeurs d'Étienne commencent à utiliser les systèmes
Linux et la ligne de commande. Afin de les aider à apprendre les
commandes, on vous charge de concevoir un logiciel qui leur
permettra de

## Livrables

Un dépôt git privé contenant:

- Un document au format Markdown expliquant votre conception. On
  veut que vous nous expliquez pourquoi vous avez structuré votre
  code de la façon dont vous l'avez fais.
- Le code source de l'application.
- Les tests de l'application.

**Veuillez voir le README à la racine du dépôt pour savoir qui
ajouté comme collaborateur.**

## Exigences applicatives

1. L'utilisateur doit pouvoir quitter le programme.
2. L'utilisateur doit voir la liste des commandes qu'il peut utiliser.
3. L'utilisateur doit pouvoir utiliser une commande.
4. Pour chaque opération qu'un utilisateur effectue, la solution doit
   afficher la ligne de commande qu'il devrait utilisé dans un
   interpréteur shell pour pour effectuer l'opération.
5. L'utilisateur doit pouvoir naviguer dans le système de fichiers.
	i. L'utilisateur doit pouvoir naviguer par chemin absolu.
	ii. L'utilisateur doit pouvoir naviguer par chemin relatif.
6. L'utilisateur doit pouvoir afficher le contenu d'un fichier.
7. La solution doit refuser l'affichage des fichiers contenant des
   caractères autres que les caractères graphiques et les espaces blancs.
8. L'utilisateur doit pouvoir créer un fichier.
9. L'utilisateur doit pouvoir créer un répertoire.
10. L'utilisateur doit pouvoir obtenir les attributs suivants d'un fichier:
	i. son nom;
	ii. sa taille;
	iii. son utilisateur propriétaire;
	iv. son groupe propriétaire;
	v. son mode d'accès;
	vi. son type.
11. L'utilisateur doit pouvoir modifier le mode d'accès d'un fichier.
12. L'utilisateur doit pouvoir modifier le mode d'accès d'un répertoire.
13. L'utilisateur doit pouvoir supprimer un fichier.
14. L'utilisateur doit pouvoir supprimer un répertoire s'il est vide
    (autrement il s'agit d'une erreur).
15. L'utilisateur doit pouvoir copier un fichier.
16. L'utilisateur doit pouvoir déplacer/renommer un fichier.
17. L'utilisateur doit pouvoir déplacer/renommer un répertoire.

## Contraintes d'implémentation

- La solution peut être écrite dans le langage que vous voulez. Des
  points bonis sont donnés aux solutions écrites en TypeScript (Node.js).
- L'utilisation de l'appel `system()` ou ses équivalents est
  strictement interdites.
- La solution doit permettre différentes interfaces utilisateur. Elle
  doit permettre de créer une interface web, en ligne de commande et
  graphique sans avoir à changer le code applicatif.

## Évaluation

Votre solution sera évaluée sur:

- la satisfaction des exigences;
- l'utilisabilité de la solution;
- la qualité générale du code de la solution;
- la couverture des tests que votre solution comprend;
- le nombre de changements nécéssaires pour complèter une modificaton
  à la solution.
